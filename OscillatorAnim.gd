# store all methods that this is controlling
const SIN = 0
const TRIANGLE = 1
const SAWTOOTH = 2
const REVERSE_SAWTOOTH = 3
export(int, "sin", "triangle", "sawtooth") var waveform = 0

export(float, 0.001, 100.0) var period = 1.0
export(float, 0.001, 100.0) var hold_period = 0.0

export(float, 0.0, 1.0) var phase_shift = 0.0
export(NodePath) var target = null setget set_target

const ALLOWED_TYPES = [TYPE_REAL, TYPE_VECTOR2, TYPE_VECTOR3, TYPE_COLOR]

var time = 0
var is_running = true


var min_val = 0
var max_val = 1
var method_name = ''
var data_type = TYPE_REAL
var _added = false
var target_node = null

func set_target(new_target_path):
    target_node = get_node(new_target_path)

#
# Set a given method of the target_node as the target as the oscillator. Will
# add to scene tree as child of target_node if not already added to scene tree.
func oscillate_method(target_node, method_name, min_val, max_val):
    # cast down to a common type
    min_val = _cast_down(min_val)
    max_val = _cast_down(max_val)
    # oscillate 

    # check that data type is as expected
    data_type = typeof(min_val)
    if data_type != typeof(max_val):
        # TODO maybe add a little leniency with float vs. int mismatch
        printerr("OscillatorAnim ERROR: Min/max types don't match")
        return
    if not ALLOWED_TYPES.has(data_type):
        printerr("OscillatorAnim ERROR: Invalid type")
        return
    self.target_node = target_node
    self.method_name = method_name
    self.min_val = min_val
    self.max_val = max_val

    if not _added:
        target_node.add_child(self)

func _cast_down(val):
    if typeof(val) == TYPE_INT:
        return float(val)
    return val

func _ready():
    _added = true
    set_process(true)

#
# Pause animation playback
func pause():
    is_running = false
    set_process(false)

#
# Fully stop playback
func stop():
    is_running = false
    set_process(false)
    time = 0

#
# Resume or start playback
func start():
    is_running = true
    set_process(true)

#
# Resets playback (without stopping) to initial state
func rewind():
    time = 0

#
# Helper function to blend from min_val to max_val based on type
func _blend(time, min_val, max_val):
    var val
    if data_type == TYPE_REAL or data_type == TYPE_VECTOR2 or data_type == TYPE_VECTOR3:
        val = ((max_val - min_val) * time) + min_val
    elif data_type == TYPE_COLOR:
        val = min_val.linear_interpolate(max_val, time)
    return val

func _process(delta):
    if target_node == null:
        return # not yet attached

    if hold_period >= period:
        printerr("OscillatorAnim ERROR hold_period must be less than period")
        return

    # Have it cycle based on delta (essentially float modulus)
    time += delta
    while time > period:
        time -= period

    # Have it "hold" at min val during hold_period
    var effective_time = (time + hold_period) / (period - hold_period)

    if time < hold_period:
        effective_time = 0

    # Now account for the phase_shift property
    effective_time += (phase_shift * period)
    while effective_time > period:
        effective_time -= period

    if waveform == SAWTOOTH:
        pass # already a sawtooth wave /|/|/|
    elif waveform == REVERSE_SAWTOOTH: # Simple inversion
        effective_time = period - effective_time
    elif waveform == TRIANGLE:
        # Conditionally reverse based on 
        if effective_time <= 0.5:
            effective_time = ((period / 2) - effective_time) * 2
        else:
            effective_time = (effective_time - (period / 2)) * 2

    elif waveform == SIN:
        # Offset sin to make it start at 0 and go to 1
        effective_time = (sin(-PI/2 + effective_time * 2 * PI) + 1) / 2

    # Call the actual function
    target_node.call(method_name, _blend(effective_time, min_val, max_val))



