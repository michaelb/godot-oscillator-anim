# godot-oscillator-anim

Super simple drop-in node node for [Godot Engine
games](http://godotengine.org/) to add simple oscillating animations, either
programmatically or in the editor, without the need of creating an entire
AnimationPlayer and a new animation.

Useful for canned / drop-in blinking, bouncing, and flickering animations, and
the like.

## Usage

Still a WIP, check out `example/` for an example of its usage.

### Programmatic usage

Assuming you checked it out to a dir called `submodules`, this will cause it to
"blink" opacity at 1 second intervals.

```
const OscillatorAnim = preload('res://submodules/godot-oscillators-anim/OscillatorAnim.gd')

func _ready():
    var my_sprite = get_node('MySprite')
    var anim = OscillatorAnim.instance()
    anim.oscillate_method(my_sprite, 'set_opacity', 0, 1)
```

