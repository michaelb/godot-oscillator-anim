const OscillatorAnim = preload('../OscillatorAnim.tscn')

onready var block = get_node('Block')
onready var block2 = get_node('Block1')
# oscilator oscillator 
var anim
var anim2

func _ready():
    anim = OscillatorAnim.instance()
    anim.oscillate_method(block, 'set_opacity', 0, 1)
    anim2 = OscillatorAnim.instance()
    anim2.oscillate_method(block2, 'set_pos', block2.get_pos(), block2.get_pos() - Vector2(0, 500))
    anim2.hold_period = 0.5

func _on_Button1_pressed():
    anim.waveform = anim.TRIANGLE
    anim2.waveform = anim.TRIANGLE

func _on_Button2_pressed():
    anim.waveform = anim.SAWTOOTH
    anim2.waveform = anim.SAWTOOTH

func _on_Button3_pressed():
    anim.waveform = anim.REVERSE_SAWTOOTH
    anim2.waveform = anim.REVERSE_SAWTOOTH

func _on_Button4_pressed():
    anim.waveform = anim.SIN
    anim2.waveform = anim.SIN
